class Test
  def initialize(input)
    @original_input = input.map(&:downcase)
    @current_input = Test.slice_input_to_place(original_input)
  end

  def execute(key = 0)
    return unless current_input[key]

    command = Command.new(current_input[key])

    if command.place?
      command.params = current_input[key + 1].split(',')

      next_key = key + 2
    else
      next_key = key + 1
    end

    robot.send(command.action, *command.params) if command.valid?

    if robot.on_table
      execute(next_key)
    else
      next_input = current_input.slice(next_key, current_input.length)
      @current_input = Test.slice_input_to_place(next_input)
      execute
    end
  end

  private

  attr_reader :original_input
  attr_accessor :current_input

  def self.slice_input_to_place(input)
    return [] unless input.index('place')
    input.slice(input.index('place')...input.length)
  end

  def robot
    @robot ||= ToyRobot.new
  end
end

class ToyRobot
  attr_accessor :x, :y, :direction, :on_table

  def initialize(x = 0, y = 0, direction: :east)
    @x, @y = x.to_i, y.to_i
    @direction = direction.downcase.to_sym
    @on_table = false
  end

  def place(x, y, direction)
    return unless Move.new(x, y, direction).allowed?

    @x, @y = x.to_i, y.to_i
    @direction = direction.downcase.to_sym
    @on_table = true
  end

  def move
    @x, @y = Move.forward(x,y,direction)
  end

  def left
    @direction = Move.left(x,y,direction)
  end

  def right
    @direction = Move.right(x,y,direction)
  end

  def jump
    @x, @y = Move.jump(x,y,direction)
  end

  def report
    puts "#{x},#{y},#{direction}"
  end
end

class Move
  DIRECTIONS = %i[east south west north]
  ROWS = 5
  COLS = 5

  def initialize(x, y, direction)
    @x, @y, @direction = x.to_i, y.to_i, direction.to_sym
  end

  def next_move(step = 1)
    {
      east: [step, 0],
      south: [0, -step],
      west: [-step, 0],
      north: [0, step]
    }
  end

  def left
    DIRECTIONS.rotate(direction_index - 1).first
  end

  def right
    DIRECTIONS.rotate(direction_index + 1).first
  end

  def forward(step = 1)
    return next_position(step) if valid_move?(next_position(step))
    return current_position
  end

  def allowed?
    valid_move?(current_position) && valid_direction?
  end

  class << self
    def left(x, y, direction)
      new(x,y,direction).left
    end

    def right(x, y, direction)
      new(x,y,direction).right
    end

    def forward(x, y, direction)
      new(x,y,direction).forward(1)
    end

    def jump(x, y, direction)
      new(x,y,direction).forward(2)
    end
  end

  private

  attr_reader :x, :y, :direction

  def current_position
    [x, y]
  end

  def next_position(step = 1)
    return current_position unless valid_direction?
    x, y = current_position
    [x + next_move(step)[direction][0], y + next_move(step)[direction][1]]
  end

  def direction_index
    DIRECTIONS.index(direction)
  end

  def valid_move?(position)
    x, y = position
    (0..ROWS).to_a.include?(x) && (0..COLS).to_a.include?(y)
  end

  def valid_direction?
    DIRECTIONS.include?(direction)
  end
end

class Command
  VALID_ACTIONS = %w(place left right move report jump)

  attr_accessor :params
  attr_reader :action

  def initialize(action, params = nil)
    @action, @params = action, params
  end

  def place?
    action == 'place'
  end

  def valid?
    VALID_ACTIONS.include?(action)
  end
end

Test.new(ARGV).execute

# ruby test.rb PLACE 0,0,NORTH MOVE REPORT
# ruby test.rb PLACE 0,0,NORTH LEFT REPORT
# ruby test.rb PLACE 1,2,EAST MOVE MOVE LEFT MOVE REPORT
# ruby test.rb CUCU PLACE 7,2,EAST MOVE MOVE LEFT MOVE REPORT
